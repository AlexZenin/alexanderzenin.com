import styled from 'styled-components';

const Heading = styled.p`
  font-size: 2em;
  font-weight: bold;
  text-transform: uppercase;
  margin-top: 200px;
`;

export default Heading;